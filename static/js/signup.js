window.onload = function () {
   document.getElementById("event").style.display = "none";
   document.getElementById("env").style.display = "none";
   document.getElementById("uatdiv").style.display = "none";
   document.getElementById("proddiv").style.display = "none";

   $('.caret').remove();
}

$(document).ready(function() {
    $('.selectpicker').selectpicker();
});

function toggle(state){
    if(state==0){
        document.getElementById("event").style.display = "none";
        document.getElementById("env").style.display = "none";
        $('form selectpicker').prop("disabled", true);
        $('form input#uaturl')[0].value="";
        $('form input#produrl')[0].value="";
    }
    else {
        document.getElementById("event").style.display = "block";
        document.getElementById("env").style.display = "block";
        $('form selectpicker').prop("disabled", false);
    }
};

function toggleuat(){
    if(document.getElementById("uat").checked == true)
        document.getElementById("uatdiv").style.display = "block";
    else {
    $('form input#uaturl')[0].value="";
    document.getElementById("uatdiv").style.display = "none";
    }
};
function toggleprod(){
    if(document.getElementById("prod").checked == true)
        document.getElementById("proddiv").style.display = "block";
    else{
        $('form input#produrl')[0].value="";
        document.getElementById("proddiv").style.display = "none";
    }

};
function upload(){
    var form = document.getElementById('clientform');
    form.onSubmit = function(event){
    event.preventDefault();
    var request = new XMLHttpRequest();
    request.open('POST', 'http://localhost:5000/signup', /* async = */ false);
    var formData = new FormData(document.getElementById('clientform'));
    request.send(formData);
    console.log(request.response);
};


};
function addEvent(node, type, callback) {
  if (node.addEventListener) {
    node.addEventListener(
      type,
      function(e) {
        callback(e, e.target);
      },
      false
    );
  } else if (node.attachEvent) {
    node.attachEvent("on" + type, function(e) {
      callback(e, e.srcElement);
    });
  }
}

//identify whether a field should be validated
//ie. true if the field is neither readonly nor disabled,
//and has either "pattern", "required" or "aria-invalid"
function shouldBeValidated(field) {
  return (
    !(field.getAttribute("readonly") || field.readonly) &&
    !(field.getAttribute("disabled") || field.disabled) &&
    (field.getAttribute("pattern") || field.getAttribute("required"))
  );
}

//field testing and validation function
function instantValidation(field) {
  //if the field should be validated
  if (shouldBeValidated(field)) {
    //the field is invalid if:
    //it's required but the value is empty
    //it has a pattern but the (non-empty) value doesn't pass
    var invalid =
      (field.getAttribute("required") && !field.value) ||
      (field.getAttribute("pattern") &&
        field.value &&
        !new RegExp(field.getAttribute("pattern")).test(field.value));

    //add or remove the attribute is indicated by
    //the invalid flag and the current attribute state
    if (!invalid && field.getAttribute("aria-invalid")) {
      field.removeAttribute("aria-invalid");
    } else if (invalid && !field.getAttribute("aria-invalid")) {
      field.setAttribute("aria-invalid", "true");
    }
  }
}

//now bind a delegated change event
//== THIS FAILS IN INTERNET EXPLORER <= 8 ==//
//addEvent(document, 'change', function(e, target)
//{
//  instantValidation(target);
//});

//now bind a change event to each applicable for field
var fields = [
  document.getElementsByTagName("input"),
  document.getElementsByTagName("textarea")
];
for (var a = fields.length, i = 0; i < a; i++) {
  for (var b = fields[i].length, j = 0; j < b; j++) {
    addEvent(fields[i][j], "change", function(e, target) {
      instantValidation(target);
    });
  }
}