var data = localStorage.getItem('data');
var username = data['ClientID'];
$(document).ready(function() {
   $('.selectpicker').selectpicker();
});

window.onload = function () {
    $('.caret').remove();
    data = JSON.parse(data);
    console.log(data);
    if(data['Data_Ingested_New']==1) $('select')[0].options[0].selected=true;
    if(data['Data_Ingested_Incremental']==1) $('select')[0].options[1].selected=true;
    if(data['Algo360_Score_Calculated']==1) $('select')[0].options[2].selected=true;
    if(data['Algo360_Process_Started']==1) $('select')[0].options[3].selected=true;
    if(data['Algo360_Process_Finished']==1) $('select')[0].options[4].selected=true;
    $('select').selectpicker("refresh");

    if(!data['isEnable'] || data['isEnable']== 0){
        document.getElementById("env").style.display = "none";
        document.getElementById("event").style.display = "none";
        document.getElementById('webhook').innerHTML = '<input type="radio"  name="webhook" value="false" onclick="toggle(0);" class="form-check-input" checked />\
                <label class="form-check-label">False</label>\
                <input type="radio" name="webhook" value="true" onclick="toggle(1);" class="form-check-input" />\
                <label class="form-check-label">True</label>'
    }
    else {
        document.getElementById("env").style.display = "block";
        document.getElementById("event").style.display = "block";
        document.getElementById('webhook').innerHTML ='<input type="radio"  name="webhook" value="false" onclick="toggle(0);" class="form-check-input" />\
                <label class="form-check-label">False</label>\
                <input type="radio" name="webhook" value="true" onclick="toggle(1);" class="form-check-input" checked/>\
                <label class="form-check-label">True</label>'
    }
    if(data['web_hook_url_uat'] && data['web_hook_url_uat'] != ""){
        document.getElementById("uatdiv").style.display = "block";
        $('form input#uat')[0].checked=true;
        $('form input#uaturl')[0].value=data['web_hook_url_uat'];
    }
    else{
        document.getElementById("uatdiv").style.display = "none";
        $('form input#uat')[0].checked=false;
    }
    if(data['web_hook_url'] && data['web_hook_url'] !=""){
        document.getElementById("proddiv").style.display = "block";
        $('form input#prod')[0].checked=true;
        $('form input#produrl')[0].value=data['web_hook_url'];
    }
    else{
        document.getElementById("proddiv").style.display = "none";
        $('form input#prod')[0].checked=false;
    }
    if(data['isCohortActive']==1){
        $('input[name=cohort]')[1].checked = true;
    }
    else $('input[name=cohort]')[0].checked = true;


}


function toggle(state){
    if(state==0){
        document.getElementById("event").style.display = "none";
        document.getElementById("env").style.display = "none";
    }
    else {
        document.getElementById("event").style.display = "block";
        document.getElementById("env").style.display = "block";
    }
};

function toggleuat(){
    if(document.getElementById("uat").checked == true)
        document.getElementById("uatdiv").style.display = "block";
    else {
    document.getElementById("uatdiv").style.display = "none";
    }
};
function toggleprod(){
    if(document.getElementById("prod").checked == true)
        document.getElementById("proddiv").style.display = "block";
    else{
        document.getElementById("proddiv").style.display = "none";
    }

};

function update(){
    var form = document.getElementById('updateform');
    form.onSubmit = function(event){
    event.preventDefault();
    var request = new XMLHttpRequest();
    request.open('POST', 'http://localhost:5000/edit/'+ username, /* async = */ false);
    var formData = new FormData(document.getElementById('updateform'));
    console.log(formData);
    request.send(formData);
    console.log(request.response);
    }
};