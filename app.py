import random
import string
import boto3
from flask import Flask, session
from flask import render_template, request, redirect, url_for, flash
from config import AWS
from config import Config

app = Flask(__name__)
app.config.from_object(Config)

dbClient = boto3.client('dynamodb', region_name='ap-south-1', aws_access_key_id=AWS.ACCESS_KEY_ID,
                        aws_secret_access_key=AWS.SECRET_ACCESS_KEY)


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        data = request.form
        username = str(data['username'])
        password = str(data['password'])
        if username is "" or password is "":
            flash('Invalid username or password', 'error')
            return redirect(url_for('index'))

        if ((username == Config.ADMIN_USERNAME) & (password == Config.ADMIN_PASSWORD)) or \
                ((username == Config.VIEW_USERNAME) & (password == Config.VIEW_PASSWORD)):
            session['username'] = username
            return redirect(url_for('admin'))
        resp = dbClient.query(TableName='ClientCredentials',
                              KeyConditions={
                                  'ClientID': {
                                      'AttributeValueList': [
                                          {
                                              'S': username
                                          }
                                      ],
                                      'ComparisonOperator': 'EQ'
                                  }
                              },
                              QueryFilter={
                                  'Password': {
                                      'AttributeValueList': [
                                          {
                                              'S': password
                                          }
                                      ],
                                      'ComparisonOperator': 'EQ'
                                  }

                              })
        if resp['Count'] == 0:
            flash('Invalid username or password', 'error')
            return redirect(url_for('index'))
        else:
            session['username'] = username
            return redirect(url_for('client', username=username))
    else:
        if 'username' in session:
            if session['username'] == Config.VIEW_USERNAME or session['username'] == Config.ADMIN_USERNAME:
                return redirect(url_for('admin'))
            else:
                return redirect(url_for('client', username=session['username']))
        return render_template('index.html')


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if 'username' in session and (session['username'] == Config.ADMIN_USERNAME or
                                  session['username'] == Config.VIEW_USERNAME):
        if request.method == 'POST':
            data = request.form
            clientid = request.form.get('clientId').lower()
            ref_code = request.form.get('refCode').upper()

            resp = dbClient.query(TableName='ClientCredentials',
                                  KeyConditions={
                                      'ClientID': {
                                          'AttributeValueList': [
                                              {
                                                  'S': clientid
                                              }
                                          ],
                                          'ComparisonOperator': 'EQ'
                                      }

                                  })
            if resp['Count'] > 0:
                flash('ClientID already exist')
                return redirect(url_for('signup'))

            client_secret = ''.join(random.choices(string.ascii_letters + string.digits, k=40)).lower()
            password = clientid + '@123'
            events = request.form.getlist('events[]')

            arr = clientid.split('_')
            username = ''
            for i in arr:
                username = username + i.capitalize()
            item_temp = {}
            item_temp['isActive'] = {'N': str(1)}
            item_temp['isTestActive'] = {'N': str(1)}
            item_temp['ClientID'] = {'S': str(clientid)}
            item_temp['ClientSecret'] = {'S': str(client_secret)}
            item_temp['Password'] = {'S': str(password)}
            item_temp['ref_code'] = {'S': str(ref_code)}
            item_temp['UserName'] = {'S': str(username)}
            if data['webhook'] == 'true':
                item_temp['isEnable'] = {'N': str(1)}
            else:
                item_temp['isEnable'] = {'N': str(0)}
            if data['cohort'] == 'true':
                item_temp['isCohortActive'] = {'N': str(1)}
            for event in events:
                item_temp[event] = {'N': str(1)}
            if data['uaturl']:
                item_temp['web_hook_url_uat'] = {'S': str(data['uaturl'])}
            if data['produrl']:
                item_temp['web_hook_url'] = {'S': str(data['produrl'])}
            print(item_temp)
            try:
                dbClient.put_item(TableName='ClientCredentials', Item=item_temp)
                flash('Successfully added')
                session['username'] = clientid
                return redirect(url_for('client', username=clientid))
            except Exception as e:
                print(e)
                flash('Error occurred')
                return redirect(url_for('signup'))

        else:
            return render_template('signup.html')
    else:
        flash('You must login first', 'error')
        return redirect(url_for('index'))


@app.route('/client/<username>', methods=['GET', 'POST'])
def client(username):
    if 'username' in session and (session['username'] == username or session['username'] == Config.ADMIN_USERNAME or
                                  session['username'] == Config.VIEW_USERNAME):
        return render_template('client.html', username=username)
    else:
        flash('You must login first', 'error')
        return redirect(url_for('index'))


@app.route('/edit/<username>', methods=['GET', 'POST'])
def edit(username):
    if 'username' in session and (session['username'] == username or session['username'] == Config.ADMIN_USERNAME):
        if request.method == 'POST':
            data = request.form
            print(data)
            item_temp = {}
            item_temp['Data_Ingested_New'] = {'Action': 'DELETE'}
            item_temp['Data_Ingested_Incremental'] = {'Action': 'DELETE'}
            item_temp['Algo360_Score_Calculated'] = {'Action': 'DELETE'}
            item_temp['Algo360_Process_Started'] = {'Action': 'DELETE'}
            item_temp['Algo360_Process_Finished'] = {'Action': 'DELETE'}
            if data['webhook'] == 'true':
                item_temp['isEnable'] = {'Value': {'N': str(1)}, 'Action': 'PUT'}
            else:
                item_temp['isEnable'] = {'Value': {'N': str(0)}, 'Action': 'PUT'}

            if data['cohort'] == 'true':
                item_temp['isCohortActive'] = {'Value': {'N': str(1)}, 'Action': 'PUT'}
            else:
                item_temp['isCohortActive'] = {'Action': 'DELETE'}

            events = request.form.getlist('events[]')
            for event in events:
                item_temp[event] = {'Value': {'N': str(1)}, 'Action': 'PUT'}
            if data['uaturl']:
                item_temp['web_hook_url_uat'] = {'Value': {'S': str(data['uaturl'])}, 'Action': 'PUT'}
            else:
                item_temp['web_hook_url_uat'] = {'Action': 'DELETE'}
            if data['produrl']:
                item_temp['web_hook_url'] = {'Value': {'S': str(data['produrl'])}, 'Action': 'PUT'}
            else:
                item_temp['web_hook_url'] = {'Action': 'DELETE'}

            print(item_temp)
            try:
                dbClient.update_item(
                    TableName='ClientCredentials',
                    Key={
                        'ClientID': {
                            'S': str(username)
                        }
                    },
                    AttributeUpdates=item_temp
                )
                flash('Successfully updated')
                return redirect(url_for('client', username=username))
            except Exception as e:
                print(e)
                flash('Could not be updated', 'error')
                return redirect(url_for('client', username=username))

        else:
            return render_template('edit.html', username=username)
    else:
        flash('You must login first', 'error')
        return redirect(url_for('index'))


@app.route('/admin')
def admin():
    if 'username' in session and (
            session['username'] == Config.ADMIN_USERNAME or session['username'] == Config.VIEW_USERNAME):
        return render_template('admin.html', username=session['username'])
    else:
        flash('You must login first', 'error')
        return redirect(url_for('index'))


@app.route('/logout')
def logout():
    if 'username' in session:
        session.pop('username', None)
    flash('Logged out successfully')
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(debug=True)
